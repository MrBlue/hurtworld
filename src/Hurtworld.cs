using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Logging;
using uMod.Plugins;
using UnityEngine;

namespace uMod.Hurtworld
{
    /// <summary>
    /// The core Hurtworld plugin
    /// </summary>
    public partial class Hurtworld : Plugin
    {
        #region Initialization

        /// <summary>
        /// Initializes a new instance of the Hurtworld class
        /// </summary>
        public Hurtworld()
        {
            // Set plugin info attributes
            Title = "Hurtworld";
            Author = HurtworldExtension.AssemblyAuthors;
            Version = HurtworldExtension.AssemblyVersion;
        }

        // Instances
        internal static readonly HurtworldProvider Universal = HurtworldProvider.Instance;

        internal bool serverInitialized;

        #endregion Initialization

        #region Core Hooks

        /// <summary>
        /// Called when the plugin is initializing
        /// </summary>
        [HookMethod("Init")]
        private void Init()
        {
            // Configure remote error logging
            RemoteLogger.SetTag("game", Title.ToLowerInvariant());
            RemoteLogger.SetTag("game version", Server.Version);
        }

        /// <summary>
        /// Called when another plugin has been loaded
        /// </summary>
        /// <param name="plugin"></param>
        [HookMethod("OnPluginLoaded")]
        private void OnPluginLoaded(Plugin plugin)
        {
            if (serverInitialized)
            {
                // Call OnServerInitialized for hotloaded plugins
                plugin.CallHook("OnServerInitialized", false);
            }
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [HookMethod("IOnServerInitialized")]
        private void IOnServerInitialized()
        {
            if (!serverInitialized)
            {
                // Add universal commands
                Universal.CommandSystem.DefaultCommands.Initialize(this);

                // Clean up invalid permission data
                permission.RegisterValidate(ExtensionMethods.IsSteamId);
                permission.CleanUp();

                // Let plugins know server startup is complete
                Interface.CallHook("OnServerInitialized", serverInitialized = true);

                Interface.uMod.LogInfo($"uMod version {uMod.Version} running on {Universal.GameName} server version {Server.Version}"); // TODO: Localization
                Analytics.Collect();

                SteamGameServer.SetGameTags("umod,modded");
            }
        }

        /// <summary>
        /// Called when the server is saved
        /// </summary>
        [HookMethod("OnServerSave")]
        private void OnServerSave()
        {
            // Trigger save process
            Interface.uMod.OnSave();
        }

        /// <summary>
        /// Called when the server is shutting down
        /// </summary>
        [HookMethod("OnServerShutdown")]
        private void OnServerShutdown()
        {
            // Trigger shutdown process
            Interface.uMod.OnShutdown();
        }

        #endregion Core Hooks

        #region Player Finding

        /// <summary>
        /// Gets the player session using a name, Steam ID, or IP address
        /// </summary>
        /// <param name="nameOrIdOrIp"></param>
        /// <returns></returns>
        public static PlayerSession Find(string nameOrIdOrIp)
        {
            PlayerSession session = null;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!nameOrIdOrIp.Equals(s.Value.Identity.Name, StringComparison.OrdinalIgnoreCase) &&
                    !nameOrIdOrIp.Equals(s.Value.SteamId.ToString()) && !nameOrIdOrIp.Equals(s.Key.ipAddress))
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a uLink.NetworkPlayer
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static PlayerSession Find(uLink.NetworkPlayer player) => GameManager.Instance.GetSession(player);

        /// <summary>
        /// Gets the player session using a UnityEngine.Collider
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static PlayerSession Find(Collider col)
        {
            PlayerSession session = null;
            EntityStats stats = col.gameObject.GetComponent<EntityStatsTriggerProxy>().Stats;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!s.Value.WorldPlayerEntity.GetComponent<EntityStats>() == stats)
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a UnityEngine.GameObject
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static PlayerSession Find(GameObject go)
        {
            Dictionary<uLink.NetworkPlayer, PlayerSession> sessions = GameManager.Instance.GetSessions();
            return (from i in sessions where go.Equals(i.Value.WorldPlayerEntity) select i.Value).FirstOrDefault();
        }

        /// <summary>
        /// Gets the player session using a Steam ID
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public static PlayerSession FindById(string playerId)
        {
            PlayerSession session = null;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!playerId.Equals(s.Value.SteamId.ToString()))
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a uLink.NetworkPlayer
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public PlayerSession Session(uLink.NetworkPlayer player) => GameManager.Instance.GetSession(player);

        /// <summary>
        /// Gets the player session using a UnityEngine.GameObject
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static PlayerSession Session(GameObject go)
        {
            return (from s in Sessions where go.Equals(s.Value.WorldPlayerEntity) select s.Value).FirstOrDefault();
        }

        /// <summary>
        /// Returns all connected sessions
        /// </summary>
        public static Dictionary<uLink.NetworkPlayer, PlayerSession> Sessions => GameManager.Instance.GetSessions();

        #endregion Player Finding
    }
}
